console.log(`Hello World`);

let num1 = parseInt(prompt(`Provide a number`));
let num2 = parseInt(prompt(`Provide another number`));

let sum = num1 + num2;


if (sum < 10) {
	console.warn(`The sum of the two numbers is: ${sum}`);
} else if (sum < 20) {
	function difference(num1, num2) {
		let smaller = Math.min(num1, num2);
		let larger = Math.max(num1, num2);
		alert(`The difference of the two numbers is: ${larger - smaller}`);
	}
	difference(num1, num2);	
} else if (sum < 30) {
	let product = num1 * num2;
	alert(`The product of the two numbers is: ${product}`);
} else if (sum >= 30) {
	function quotient(num1, num2) {
		let smaller = Math.min(num1, num2);
		let larger = Math.max(num1, num2);
		alert(`The quotient of the two numbers is: ${larger / smaller}`);
	}
	quotient(num1, num2);
} 


let name = prompt(`What is your name?`);
let age = parseInt(prompt(`What is your age?`));

if (name && age) {
	alert(`Hello ${name}. Your age is ${age}.`)
} else {
	alert(`Are you a time traveller?`)
}

function isLegalAge(age) {
	let legalAge = (age < 18)? alert(`You are not allowed here.`) : alert(`You are of legal age.`);
}

isLegalAge(age);

switch (age) {
	case 18:
		alert(`You are now allowed to party.`);
		break;
	case 21:
		alert(`You are now part of the adult society.`);
		break;
	case 65:
		alert(`We thank you for your contribution to society.`);
		break;
	default:
		alert(`Are you sure you're not an alien?`);
		break;
}


try {
  isLegalAg(age);
}
catch(error) {
  console.warn(error.message);
}
finally {
  isLegalAge(age);
}








